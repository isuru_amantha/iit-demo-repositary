﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Azure_Testing.Startup))]
namespace Azure_Testing
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
